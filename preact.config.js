import { p3dConfig } from 'p3d-preact-plugin'
import webpack from 'webpack'

export default function (config, env, helpers) {
    p3dConfig(config, env, helpers)

    const PUBLIC_PATH = process.env.PUBLIC_PATH || '/'
    config.output.publicPath = PUBLIC_PATH
    config.plugins.push(new webpack.DefinePlugin({ PUBLIC_PATH }))
}
