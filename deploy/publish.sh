#!/bin/bash
set -ex

BUCKET=demo.p3d.in
if [ -n "$BITBUCKET_REPO_SLUG" ]; then
    NAME=$BITBUCKET_REPO_SLUG
    BRANCH=$BITBUCKET_BRANCH
else
    NAME=$(basename `git rev-parse --show-toplevel`)
    BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi
PREFIX=$BRANCH/$NAME


echo "deploying to $BUCKET/$PREFIX ..."

rm -rf dist_publish
cp -r build dist_publish

for f in dist_publish/*.js; do
    if [ -f "$f.gz" ]; then
        echo "$f -> $f.gz"
        rm $f
        # mv $f.gz $f
    fi
done

s3cmd sync -P --no-mime-magic --delete-removed --recursive --exclude '*.gz' dist_publish/ s3://$BUCKET/$PREFIX/

for f in build/*.js; do
    if [ -f "$f.gz" ]; then
        key=$(basename $f)
        s3cmd sync -P --add-header="Content-Encoding: gzip"\
            --add-header="Content-Type: application/javascript"\
            $f.gz s3://$BUCKET/$PREFIX/$key
    fi
done