import './style'
import App from './app'

export default function() {
    return (
        <div class="container">
            <App />
        </div>
    )
}
