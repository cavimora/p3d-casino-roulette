import * as THREE from 'three'

class Input {
    constructor() {
        this.mouse = {
            down: false,
            justPressed: false,
            justReleased: false,
            position: new THREE.Vector2(),
            normalizedPosition: new THREE.Vector2(),
            dragStart: new THREE.Vector2(),
            lastPosition: new THREE.Vector2()
        }
        this.raycaster = new THREE.Raycaster()
        this.listeners = []
        this.camera = null

        document.addEventListener('mousedown', e => {
            this.mouseDown(e)
        })
        document.addEventListener('mousemove', e => {
            this.mouseMoved(e)
        })
        document.addEventListener('mouseup', e => {
            this.mouseUp(e)
        })
    }

    update() {
        this.mouse.justPressed = false
        this.mouse.justReleased = false
        let mouseHandled = false
        if (this.listeners.length > 0) {
            const intersects = this.getMouseIntersection(
                this.camera,
                this.listeners
            )

            // let mouse = this.mouse.normalizedPosition;

            // let vector = new THREE.Vector3( mouse.x, mouse.y, 1 );
            // vector = vector.unproject(camera);
            // // projector.unprojectVector( vector, camera );
            // let ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

            // // create an array containing all objects in the scene with which the ray intersects
            // let intersects = ray.intersectObjects( this.listeners );
            // if there is one (or more) intersections
            for (let i = 0; i < intersects.length; i++) {
                if (intersects[i].object.handleMouse) {
                    intersects[i].object.handleMouse(this, intersects[i])
                    mouseHandled = true
                }
            }
        }

        return mouseHandled
    }

    getMouseIntersection(camera, objects) {
        // Update raycast
        this.raycaster.setFromCamera(this.mouse.normalizedPosition, camera)

        // create an array containing all objects in the scene with which the ray intersects
        return this.raycaster.intersectObjects(objects)

        // // if there is one (or more) intersections
        // if (intersects.length > 0) {
        //     // console.log(intersects);
        //     return intersects[0];
        // }
        // return null;
    }

    mouseDown(e) {
        e.preventDefault()
        this.mouse.down = true
        this.mouse.justPressed = true

        this.mouse.position.x = e.x
        this.mouse.position.y = e.y
        this.mouse.normalizedPosition.x = e.clientX / window.innerWidth * 2 - 1
        this.mouse.normalizedPosition.y =
            -(e.clientY / window.innerHeight) * 2 + 1

        this.mouse.dragStart.x = this.mouse.normalizedPosition.x
        this.mouse.dragStart.y = this.mouse.normalizedPosition.y
        this.mouse.lastPosition.x = this.mouse.normalizedPosition.x
        this.mouse.lastPosition.y = this.mouse.normalizedPosition.y
    }

    mouseUp(e) {
        e.preventDefault()
        this.mouse.down = false
        this.mouse.justReleased = true
    }

    mouseMoved(e) {
        e.preventDefault()
        this.mouse.lastPosition.x = this.mouse.normalizedPosition.x
        this.mouse.lastPosition.y = this.mouse.normalizedPosition.y
        this.mouse.position.x = e.x
        this.mouse.position.y = e.y
        this.mouse.normalizedPosition.x = e.clientX / window.innerWidth * 2 - 1
        this.mouse.normalizedPosition.y =
            -(e.clientY / window.innerHeight) * 2 + 1
    }

    wasDragging() {
        return (
            this.mouse.normalizedPosition.distanceTo(this.mouse.dragStart) > 0.1
        )
    }

    getDragDelta() {
        return new THREE.Vector2(
            this.mouse.normalizedPosition.x - this.mouse.lastPosition.x,
            this.mouse.normalizedPosition.y - this.mouse.lastPosition.y
        )
    }

    addMouseListener(obj) {
        if (this.listeners.indexOf(obj) === -1) this.listeners.push(obj)
    }

    removeMouseListener(obj) {
        this.listeners.splice(this.listeners.indexOf(obj), 1)
    }
}

export default Input
