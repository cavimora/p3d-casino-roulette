import * as THREE from 'three'
import * as TWEEN from 'es6-tween'
import { P3dMesh, P3dModelLoader } from 'p3d-three'
import P3dBaseApp from 'p3d-preact-plugin/components/p3d-base'
import pkg from '../package.json'
import UITitle from './uititle'
import Input from './input'
import REWARDS from './rewards.json'
import * as Util from './util'

const MESH_ID_WHEEL = '3KeqX'
const MESH_ID_BALL = 'DQ3pM'//'gULdu'
const MESH_ID_HANDLE = 'dJc6C'
const WHEEL_SLOT_COUNT = 26
const WHEEL_REWARD_DISTANCE = 0.765

const AppState = {
    Title: 0,
    Spinning: 1,
    ShowingReward: 2
}

export default class App extends P3dBaseApp {
    constructor() {
        super(pkg)
    }

    async setupScene() {
        this.state = AppState.Title

        // make transparency work a bit better
        this.renderer.sortObjects = false

        // Create camera
        this.camera.position.y = 1.5
        this.camera.position.z = 2.5
        this.camera.lookAt(new THREE.Vector3())

        this.modelLoader = new P3dModelLoader()

        // Create wheel of fortune
        this.wheelGroup = new THREE.Group()
        this.scene.add(this.wheelGroup)
        const wheel = new P3dMesh()
        wheel.autoCenter = false
        this.modelLoader.load(wheel, MESH_ID_WHEEL)
        this.wheelGroup.add(wheel)

        this.ballGroup =  new THREE.Group()
        this.scene.add(this.ballGroup)
        this.ball = new P3dMesh()
        this.ball.autoCenter = false
        this.modelLoader.load(this.ball, MESH_ID_BALL)
        this.ball.scale.set(0.05, 0.05, 0.05)
        this.ball.position.set(0.0, 0.5, 0.75)
        //this.ball.rotation.z = 2 * Math.PI / 3
        this.ballGroup.add(this.ball)
        

        // this.handle = new P3dMesh()
        // this.handle.autoCenter = false
        // this.modelLoader.load(this.handle, MESH_ID_HANDLE)
        // this.handle.scale.set(0.05, 0.05, 0.05)
        // this.handle.position.set(0, 0.12, 1.0)
        

        this.loadRewards()

        await this.modelLoader.wait()

        // Group for showing reward
        this.rewardGroup = new THREE.Group()
        this.rewardGroup.position.y = 0.3
        this.rewardGroup.scale.setScalar(1.3)
        this.scene.add(this.rewardGroup)

        // Load the background texture
        const textureLoader = new THREE.TextureLoader()
        const texture = textureLoader.load('assets/BG2.jpg')
        const backgroundMesh = new THREE.Mesh(
            new THREE.PlaneGeometry(2, 2, 0),
            new THREE.MeshBasicMaterial({
                map: texture,
                depthTest: false,
                depthWrite: false
            })
        )

        // Create your background scene
        this.backgroundScene = new THREE.Scene()
        this.backgroundCamera = new THREE.Camera()
        this.backgroundScene.add(this.backgroundCamera)
        this.backgroundScene.add(backgroundMesh)

        // Add a ui canvas
        this.foregroundScene = new THREE.Scene()
        this.foregroundCamera = new THREE.Camera()

        this.uiTitle = new UITitle()
        this.uiTitle.scale.set(
            1.0 / this.canvas.width,
            1.0 / this.canvas.height,
            1
        )
        this.foregroundScene.add(this.uiTitle)

        // Initialize input
        this.input = new Input()
        this.input.camera = this.camera
        this.inputEnabled = true
        this.isDragging = false

        this.rotationSpeed = 0

        //Axis Helper
        this.scene.add( new THREE.AxisHelper( 20 ) )
        this.ballGroupRotationTotal = 0
        this.ballGroupTotalChanges = 0
        
        this.light = new THREE.PointLight( 0xffffff, 7, 100 )
        this.light.position.set( 0, 0, 10 )
        this.scene.add( this.light )
    }

    resize() {
        super.resize()
        // if (this.backgroundCamera) {
        //     this.backgroundCamera.aspect =
        //         this.canvas.clientWidth / this.canvas.clientHeight
        //     this.backgroundCamera.updateProjectionMatrix()
        // }
        // if (this.foregroundCamera) {
        //     this.foregroundCamera.aspect =
        //         this.canvas.clientWidth / this.canvas.clientHeight
        //     this.foregroundCamera.updateProjectionMatrix()
        // }
        if (this.uiTitle) {
            this.uiTitle.scale.set(
                1.0 / this.canvas.width,
                1.0 / this.canvas.height,
                1
            ) // 0.00097, 1)
        }
    }

    beforeRender() {
        this.renderer.autoClear = false
        this.renderer.clear()
        this.renderer.render(this.backgroundScene, this.backgroundCamera)
    }

    afterRender() {
        this.renderer.render(this.foregroundScene, this.foregroundCamera)
    }

    update(dt) {
        this.uiTitle.update()

        switch (this.state) {
        case AppState.Title:
            if (this.input.mouse.justReleased && this.input.wasDragging()) {
                this.startSpinning()
            }
            break
        case AppState.Spinning:
            // Decay rotation speed
            if (this.rotationSpeed < 0) {
                this.rotationSpeed *= 0.99

                this.wheelGroup.rotation.y += this.rotationSpeed

                this.animateHighlightedReward()
                this.animateBall()
            }
            // If we stopped (or close enough), show reward
            if (Math.abs(this.rotationSpeed) < 0.0006) {
                this.rotationSpeed = 0
                this.showReward()
            }
            break
        case AppState.ShowingReward:
            if (this.rewardMesh) {
                if (this.input.mouse.down && this.input.wasDragging()) {
                    Util.rotateOnWorldAxis(
                        this.rewardMesh,
                        new THREE.Vector3(0, 1, 0),
                        this.input.getDragDelta().x
                    )
                    Util.rotateOnWorldAxis(
                        this.rewardMesh,
                        new THREE.Vector3(-1, 0, 0),
                        this.input.getDragDelta().y
                    )
                }
                else {
                    this.rewardMesh.rotation.y +=
                            THREE.Math.DEG2RAD * 20 * dt
                }
            }
            if (
                this.input.mouse.justReleased &&
                    !this.input.wasDragging()
            ) {
                this.restart()
            }
            break
        }

        this.input.update()
        TWEEN.update()
    }
    
    define3DText(font){
        //console.log(font)
        for (let i = 0; i < WHEEL_SLOT_COUNT; i++) {


            // const p3dModel = new P3dMesh()
            // p3dModel.autoCenter = false
            // p3dModel.autoScale = true
            //this.modelLoader.load(p3dModel, REWARDS[i].id)
            const number = new THREE.TextGeometry(i, { font } )
            const parent = new THREE.Group()
            parent.scale.set(0.2, 0.2, 0.2)
            const material = new THREE.MeshLambertMaterial({
                color: 0xdddddd
            })
            const textMesh = new THREE.Mesh( number, material );
            parent.add(textMesh)
            parent.position.y = 0.06
            parent.position.x = 0
            //parent.position.y = REWARDS[i].y
            textMesh.scale.set(0.005,0.005,0.005)
            this.rewardModels[i] = textMesh
            this.wheelGroup.add(parent)
            this.rewardSprites[i] = parent
        }
        this.placeRewardsRandomly()
    }
    
    async loadRewards() {
        this.rewardSprites = new Array(REWARDS.length).fill(null)
        this.rewardModels = new Array(REWARDS.length).fill(null)
        const fontLoader = new THREE.FontLoader()
        await fontLoader.load('helvetiker_regular.typeface.js', async (font) => {
            this.define3DText(font)
        })
        //this.slotNumber = new UI.Element()
        // Load all rewards
        
    }

    placeRewardsRandomly() {
        const deltaAngle = 360 / WHEEL_SLOT_COUNT
        this.slots = new Array(WHEEL_SLOT_COUNT).fill(null)
        for (let i = 0; i < this.rewardSprites.length; i++) {
            // Pick proper slot
            // let slot = 0
            // do {
            //     slot = THREE.Math.randInt(0, WHEEL_SLOT_COUNT - 1)
            // } while (this.slots[slot] !== null)

            // Place the sprite
            const sprite = this.rewardSprites[i]
            const rad = THREE.Math.DEG2RAD * (90 + i * deltaAngle)
            sprite.position.x = Math.cos(rad) * WHEEL_REWARD_DISTANCE
            sprite.position.z = Math.sin(rad) * WHEEL_REWARD_DISTANCE
            sprite.reward = i
            this.slots[i] = sprite
        }
    }

    startSpinning() {
        this.state = AppState.Spinning
        this.rotationSpeed = Math.min(-0.05, this.input.getDragDelta().x * 3)
        this.uiTitle.popTitle()
        new TWEEN.Tween(this.uiTitle.material)
            .to({ opacity: 0 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .start()


        // this.uiTitle.visible = false
        // new TWEEN.Tween(this.camera.position)
        //     .to({ y: 0.3, z: 1.12 })
        //     .easing(TWEEN.Easing.Quadratic.InOut)
        //     .start()


        new TWEEN.Tween(this.ball.position)
            .to({ y: 0.09 })
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .start()


        // const targetPosition = new THREE.Vector3()
        // new TWEEN.Tween(targetPosition)
        //     .to({ z: 0.65 })
        //     .easing(TWEEN.Easing.Quadratic.InOut)
        //     .on('update', () => {
        //         this.camera.lookAt(targetPosition)
        //     })
        //     .start()
    }

    async showReward() {
        this.state = AppState.ShowingReward

        // Calculate picked slot
        let wheelAngle =
            Math.round(THREE.Math.radToDeg(this.wheelGroup.rotation.y)) % 360
        let ballAngle =
            Math.round(THREE.Math.radToDeg(this.ballGroup.rotation.y)) % 360

        if (wheelAngle < 0) wheelAngle += 360
        if (ballAngle < 0) ballAngle += 360

        const realWheelAngle = 360 - wheelAngle
        const realBallAngle = 360 - ballAngle

        const delta = 360 / WHEEL_SLOT_COUNT
        const test2 = Math.abs(realWheelAngle - realBallAngle) % 360
        this.pickedSlot = Math.round(test2 / delta)
        console.log(this.pickedSlot)
        // Check if we have a slot
        if (this.slots[this.pickedSlot] !== null) {
            const rewardIdx = this.slots[this.pickedSlot].reward
            //const reward = REWARDS[rewardIdx]
            this.rewardMesh = this.rewardModels[rewardIdx]
            //await this.rewardMesh.pendingTextures

            this.rewardGroup.add(this.rewardMesh)

            this.uiTitle.showReward("")

            //this.wheelGroup.visible = false
            //this.handle.visible = false
        }
        else {
            this.uiTitle.showReward()
        }
        this.uiTitle.material.opacity = 1

        new TWEEN.Tween(this.camera.position)
            .to({ y: 1.5, z: 2.5 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .start()

        const targetPosition = new THREE.Vector3()
        new TWEEN.Tween(targetPosition)
            .to({ z: 0 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', () => {
                this.camera.lookAt(targetPosition)
            })
            .start()
    }

    restart() {
        this.state = AppState.Title
        if (this.rewardMesh) {
            const rewardIdx = this.slots[this.pickedSlot].reward
            this.rewardSprites[rewardIdx].add(this.rewardMesh)
            this.rewardMesh.rotation.set(0, 0, 0)
            //this.scene.remove(this.rewardMesh)
            this.rewardMesh = null
        }
        this.uiTitle.showTitle()

        this.wheelGroup.visible = true
        //this.handle.visible = true
        new TWEEN.Tween(this.uiTitle.material).to({ opacity: 1 }).start()

        new TWEEN.Tween(this.ball.position)
            //this.ball.position.set(0.0, 0.5, 0.75)
            .to({ x: 0.0, y: 0.5, z: 0.75 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .start()
        this.rotationSpeed = Math.min(-0.05, this.ballGroupRotationTotal * 3)
        this.returnBall()
        // new TWEEN.Tween(this.ballGroup.position)
        //     .to({ x: 0, y: 0.3, z: 0.8 })
        //     .easing(TWEEN.Easing.Quadratic.InOut)
        //     .start()
    }

    animateHighlightedReward() {
        // Calculate picked slot
        let wheelAngle =
            Math.round(THREE.Math.radToDeg(this.wheelGroup.rotation.y)) % 360
        let ballAngle =
            Math.round(THREE.Math.radToDeg(this.ballGroup.rotation.y)) % 360

        if (wheelAngle <= 0) wheelAngle += 360
        if (ballAngle <= 0) ballAngle += 360

        const realWheelAngle = Math.abs(360 - wheelAngle)
        const realBallAngle = Math.abs(360 - ballAngle)

        const delta = Math.abs(360 / WHEEL_SLOT_COUNT)
        const test2 = Math.abs(realWheelAngle - realBallAngle) % 360
        const focusedSlot = Math.round(test2 / delta)
        // if (focusedSlot !== this.focusedSlot) {
        //     this.focusedSlot = focusedSlot
        //     if (this.slots[focusedSlot]) {
        //         new TWEEN.Tween(this.slots[focusedSlot].scale)
        //             .to({ x: 0.25, y: 0.25, z: 0.25 }, 200)
        //             .easing(TWEEN.Easing.Quadratic.InOut)
        //             .yoyo(true)
        //             .repeat(1)
        //             .start()
        //     }
        // }
    }

    returnBall(){
        let angle = Math.round(THREE.Math.radToDeg(this.ballGroup.rotation.y)) % 360
        if (angle < 0) angle += 360
        console.log('ball final:', angle)
        
        const rotation = this.ballGroupRotationTotal / this.ballGroupTotalChanges
        while (this.ballGroupTotalChanges > 0) {
            //debugger
            //this.rotationSpeed *= 0.99
            this.ballGroupTotalChanges --
            //this.ballGroupRotationTotal += this.rotationSpeed
            this.ballGroup.rotation.y = rotation
        }
        this.ballGroupRotationTotal = 0
        //this.rotationSpeed = 0
        // angle = Math.round(THREE.Math.radToDeg(this.ballGroup.rotation.y)) % 360
        // if (angle < 0) angle += 360
        // console.log('ball final:', angle)
        
    }
    

    animateBall() {
        let angle = Math.round(THREE.Math.radToDeg(this.ballGroup.rotation.y)) % 360
        if (angle < 0) angle += 360
        
        // console.log('ball:', angle)
        this.ball.rotation.z -= this.rotationSpeed * 3
        this.ball.rotation.y -= this.rotationSpeed * 3
        this.ball.rotation.x -= this.rotationSpeed * 3
        this.ballGroupRotationTotal -= this.rotationSpeed * 1.5
        this.ballGroupTotalChanges++
        this.ballGroup.rotation.y = this.ballGroupRotationTotal
        //const delta = 360 / WHEEL_SLOT_COUNT
        //const focusedSlot = Math.round(angle / delta)
        //const offset = angle / delta - focusedSlot
        //this.handle.rotation.y = offset * 45 * THREE.Math.DEG2RAD
    }
}
