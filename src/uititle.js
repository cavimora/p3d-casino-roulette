import * as TWEEN from 'es6-tween'
import { UI } from 'p3d-three'

class UITitle extends UI.Canvas {
    constructor() {
        super(2048, 2048)

        //-------------------------------------
        // Title
        //-------------------------------------
        this.titleGroup = new UI.Element()
        this.addUIElement(this.titleGroup)

        const logo = new UI.Image('assets/casino-royale.png', 693, 332)
        logo.position.set(1024, 370)
        logo.anchor.set(0.5, 0.5)
        this.titleGroup.addChild(logo)
        // this.addUIElement(logo)

        this.titleText = this.createText(
            this.width / 2,
            560,
            'CASINO ROYALE P3D EDITION',
            {
                fontSize: 53,
                style: 'bold',
                font: 'Roboto',
                alignment: 'left',
                color: 'white'
            }
        )
        this.titleText.anchor.set(0.5, 0.5)
        this.titleGroup.addChild(this.titleText)
        // this.addUIElement(this.titleText)

        //-------------------------------------
        // Reward
        //-------------------------------------
        this.rewardGroup = new UI.Element()
        this.rewardGroup.visible = false
        this.addUIElement(this.rewardGroup)

        this.congratsTitle = this.createText(1024, 513, 'CONGRATULATIONS!', {
            fontSize: 53,
            style: 'bold',
            font: 'Roboto',
            alignment: 'center',
            color: 'yellow'
        })
        //this.congratsTitle.anchor.set(0.5, 0.5)
        this.rewardGroup.addChild(this.congratsTitle)

        this.rewardText = this.createText(
            1024,
            560,
            'YOU WON A STRATUS XL GAMEPAD',
            {
                fontSize: 53,
                style: 'bold',
                font: 'Roboto',
                alignment: 'center',
                color: 'white'
            }
        )
        //this.rewardText.anchor.set(0.5, 0.5)
        this.rewardGroup.addChild(this.rewardText)
    }

    createPanel(x, y, w, h, color) {
        const r = new UI.Rect(w, h, color)
        r.position.set(x, y)
        return r
    }

    createImage(x, y, width, height, url) {
        const i = new UI.Image(url, width, height)
        i.position.set(x, y)
        return i
    }

    createText(x, y, text, style) {
        const label = new UI.Label(text, {
            style: style.style,
            fontSize: style.fontSize,
            font: style.font,
            textAlign: style.alignment,
            fillStyle: style.color,
            multiline: style.multiline || false,
            height: style.multiline ? style.height : style.fontSize,
            width: style.width,
            leading: style.leading
        })
        label.position.set(x, y)

        // Testing: Add a random color when clicking a label
        // label.inputEnabled = true
        // label.inputEvents.pressed = () => {
        //     let color = new THREE.Color(0xffffff)
        //     color.setHex(Math.random() * 0xffffff)
        //     label.fillStyle = '#' + color.getHexString()
        //     this.dirty = true
        // }
        return label
    }

    showCanvas(time) {
        time = time || 500
        const tween = new TWEEN.Tween(this.plane.material)
            .to({ opacity: 1 }, time)
            .easing(TWEEN.Easing.Quadratic.InOut)
        tween.start()
    }

    hideCanvas(time) {
        time = time || 500
        const tween = new TWEEN.Tween(this.material)
            .to({ opacity: 0 }, time)
            .easing(TWEEN.Easing.Quadratic.InOut)
        tween.start()
    }

    popTitle() {
        this.titleTween = new TWEEN.Tween(this.titleText.scale)
            .to({ x: 1.2, y: 1.2 }, 200)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .yoyo(true)
            .repeat(1)
            .on('update', () => {
                this.dirty = true
            })
            .start()
    }

    showReward(rewardName) {
        this.titleGroup.visible = false
        this.rewardGroup.visible = true
        if (rewardName) {
            this.congratsTitle.text = 'CONGRATULATIONS!'
            this.rewardText.text = 'YOU WON A ' + rewardName
        }
        else {
            this.congratsTitle.text = 'BETTER LUCK NEXT TIME'
            this.rewardText.text = 'TAP TO TRY AGAIN'
        }
        this.dirty = true
    }

    showTitle() {
        this.titleGroup.visible = true
        this.rewardGroup.visible = false
    }
}

export default UITitle
