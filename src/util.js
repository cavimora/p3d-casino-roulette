import * as THREE from 'three'
const q = new THREE.Quaternion()

export function rotateOnWorldAxis(obj, axis, angle) {
    q.setFromAxisAngle(axis, angle)
    obj.applyQuaternion(q)
}
